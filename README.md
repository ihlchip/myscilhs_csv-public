#Prerequisites

The myscilhs_csv project is a Java7 project that uses Maven to be built into a war file. This war 
file is to be run on a Tomcat7 instance. Java7, Maven, and Tomcat7 should be installed before 
building this project.

    sudo apt-get install java7 tomcat7 maven

#Building the war:

Create a workspace for the project, then use git to fetch the code. Finally use Maven to build.

    mkdir myscilhs_csv ; cd myscilhs_csv
    git clone git@github.com:chb/myscilhs_csv.git
    mvn clean compile package

#Deploying the war:

Copy the war in the target directory to your tomcat webapps directory and restart tomcat.

    sudo cp target/myscilhs_csv.war /var/lib/tomcat7/webapps/.
    sudo chown tomcat7:tomcat7 /var/lib/tomcat7/webapps/myscilhs-csv.war
    sudo service tomcat7 restart
    
#Login to the application

    http://localhost:8080/myscilhs-csv/login.html
    
#Configuration

/src/main/resources/myscilhs_csv.properties
 - Output file save location
 - mysql host
 - mysql port
 - mysql username
 - mysql password
 - app user
 - app password
