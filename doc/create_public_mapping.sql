CREATE SCHEMA IF NOT EXISTS `myscilhs`;

USE `myscilhs`;

CREATE TABLE IF NOT EXISTS `public_id_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` varchar(255) NOT NULL,
  `i2b2_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `public_idx` (`public_id`),
  INDEX `i2b2_idx` (`i2b2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
