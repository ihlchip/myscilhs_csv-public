/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.pojo;

import java.util.Date;

/**
 * Created by paul
 */
public class Patient {

    //not null
    private final String patientExtId;
    private final String firstName;
    private final String lastName;
    //can be null
    private final String middleName;
    private final String homePhone;
    private final String dayPhone;
    private final String email;
    private final String address1;
    private final String address2;
    private final String city;
    private final String state;
    private final String zipCode;
    private final String country;
    private final Date dateOfBirth;

    public Patient(String patientExtId, String firstName, String lastName,
                   String middleName, String homePhone, String dayPhone, String email, String address1,
                   String address2, String city, String state, String zipCode, String country, Date dateOfBirth) {

        this.patientExtId = patientExtId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.homePhone = homePhone;
        this.dayPhone = dayPhone;
        this.email = email;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.country = country;
        this.dateOfBirth = dateOfBirth;
    }

    public String getPatientExtId() {
        return patientExtId;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public String getHomePhone() {
        return homePhone;
    }
    public String getDayPhone() {
        return dayPhone;
    }
    public String getEmail() {
        return email;
    }
    public String getAddress1() {
        return address1;
    }
    public String getAddress2() {
        return address2;
    }
    public String getCity() {
        return city;
    }
    public String getState() {
        return state;
    }
    public String getZipCode() {
        return zipCode;
    }
    public String getCountry() {
        return country;
    }
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientExtId='" + patientExtId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", homePhone='" + homePhone + '\'' +
                ", dayPhone='" + dayPhone + '\'' +
                ", email='" + email + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", country='" + country + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
