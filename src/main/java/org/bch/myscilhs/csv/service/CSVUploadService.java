/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.service;

import org.bch.myscilhs.csv.pojo.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.Resource;

/**
 * Parses the file into Patients, then sends the patients to the PublicIdDao for public id mappings
 * then creates a redcap compatible file to be sent to a configurable location.
 */
@Service
public class CSVUploadService {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private static final String[] defaultColumnNames = {"empi", "mrn_type", "first_name",
            "last_name", "middle_name", "date_of_birth", "home_phone", "day_phone", "email_address", "address_1",
            "address_2", "city", "state", "zip", "country"};
    static final String DEFAULT_DELIMITER = ",";

    private static Logger logger = LoggerFactory.getLogger(CSVUploadService.class);

  /**
     * Takes in a patient CSV file and parses the header to determine the columns of the data needed. Then it processes
     * each row and tries to create a patient object out of it.
     *
     * @param patientFile
     * @param configFile
     * @return
     */
    public PatientFileResult loadPatientFile(MultipartFile patientFile, String configFile) throws IOException {
        long counter = 0;
        Map<String, Integer> headerToIndexMap = null;
        Set<Integer> unknownIndexes;
        PatientFileResult result = new PatientFileResult(new LinkedHashMap<Long, String>(), new ArrayList<Patient>());

        if(patientFile == null || patientFile.isEmpty()){
            result.getFileLineToErrorsMap().put(counter, "CSV File was null or empty");
            return result;
        }

        //Read the CSV file
        InputStream inputStream = patientFile.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            // the second param is size. The default is 0 which discards trailing empty tokens from the array.
            String[] tokens = line.split(DEFAULT_DELIMITER, -1);
            if (++counter == 1) {
                cleanHeader(tokens);

                if(tokens.length < 2){
                    result.getFileLineToErrorsMap().put(counter, "Unexpected Delimiter used in CSV file. Expected \""
                            + DEFAULT_DELIMITER + "\"");
                    return result;
                }

                headerToIndexMap = parseHeader(tokens, configFile);

                if(headerToIndexMap == null || headerToIndexMap.isEmpty()
                        || !validateHeaders(headerToIndexMap, result)) {
                    logger.error("HeaderToIndexMap: " + headerToIndexMap);
                    result.getFileLineToErrorsMap().put(counter, "Could not find required headers.");
                    return result;
                }

                Collection<Integer> knownIndexes = headerToIndexMap.values();
                unknownIndexes = new HashSet<Integer>();
                for(int i = 0; i < tokens.length; i++){
                    if(!knownIndexes.contains(i)){
                        unknownIndexes.add(i);
                    }
                }
            } else {
                try {
                    cleanRow(tokens);

                    String patientExtId = getColumnValue(tokens, defaultColumnNames[0], headerToIndexMap, true);
                    //patientExtId = patientExtId + getColumnValue(tokens, defaultColumnNames[1], headerToIndexMap, true);
                    String firstName = getColumnValue(tokens, defaultColumnNames[2], headerToIndexMap, true);
                    String lastName = getColumnValue(tokens, defaultColumnNames[3], headerToIndexMap, true);
                    String middleName = getColumnValue(tokens, defaultColumnNames[4], headerToIndexMap, false);
                    String homePhone = getColumnValue(tokens, defaultColumnNames[6], headerToIndexMap, false);
                    String dayPhone = getColumnValue(tokens, defaultColumnNames[7], headerToIndexMap, false);
                    logger.debug("email index" +defaultColumnNames[8]+ " index map " +headerToIndexMap.toString());
                    String email = getColumnValue(tokens, defaultColumnNames[8], headerToIndexMap, true);
                    String address1 = getColumnValue(tokens, defaultColumnNames[9], headerToIndexMap, false);
                    String address2 = getColumnValue(tokens, defaultColumnNames[10], headerToIndexMap, false);
                    String city = getColumnValue(tokens, defaultColumnNames[11], headerToIndexMap, false);
                    String state = getColumnValue(tokens, defaultColumnNames[12], headerToIndexMap, false);
                    String zipCode = getColumnValue(tokens, defaultColumnNames[13], headerToIndexMap, false);
                    String country = getColumnValue(tokens, defaultColumnNames[14], headerToIndexMap, false);

                    String dob = getColumnValue(tokens, defaultColumnNames[5], headerToIndexMap, true);
                    Date birthDate = null;
                    if(dob != null && !dob.isEmpty()){
                        try {
                            birthDate = sdf.parse(dob);
                        }catch (ParseException pe) {
                            logger.error("Could not read date of birth. Patient still loaded.");
                            result.getFileLineToErrorsMap().put(
                                    counter, "Could not read date of birth. Patient still loaded.");
                        }
                    }
                    // validate contact information and skip this patient if not found.
                    if(!verifyOneExists(homePhone, dayPhone, email, address1)){
                        result.getFileLineToErrorsMap().put(counter,
                                "Patient contact information missing. One of the following headers must have a value "
                                        + defaultColumnNames[6]
                                        + ", " + defaultColumnNames[7]
                                        + ", " + defaultColumnNames[8]
                                        + ", " + defaultColumnNames[9]
                                        + ". Patient will not be loaded.");
                        continue;
                    }

                    // loop through siteSpecific indexes for the patient
                    // if we need to save this data

                    // create patient
                    Patient patient = new Patient(patientExtId, firstName, lastName, middleName, homePhone, dayPhone,
                            email, address1, address2, city, state, zipCode, country, birthDate);

                    result.getPatientList().add(patient);
                } catch (Exception e) {
                    logger.error("Error loading patient: ",e);
                    result.getFileLineToErrorsMap().put(counter, e.getMessage());
                }
            }
        }
        return result;
    }

    /**
     *
     * Normalizes each token to avoid common typographical errors
     *
     * @param headerRow
     */
    protected static void cleanHeader(final String[] headerRow){
        if(headerRow != null && headerRow.length > 0){
            for(int i = 0; i < headerRow.length; i++){
                headerRow[i] = headerRow[i].toLowerCase().trim();
            }
        }
    }

    /**
     *
     * Normalizes each token to avoid common typographical errors
     *
     * @param row
     */
    protected static void cleanRow(final String[] row){
        if(row != null && row.length > 0){
            for(int i = 0; i < row.length; i++){
                row[i] = row[i].trim();
            }
        }
    }

    /**
     *
     * Finds each header matching known headers and put the index in result map
     *
     * @param headerRow
     * @param configFileName
     * @return map of header name to token index
     */
    protected static Map<String, Integer> parseHeader(final String[] headerRow, String configFileName) {
        Map<String, Integer> headerToIndexMap = new HashMap<String, Integer>();
        String [] columnNames = defaultColumnNames;

        if(configFileName != null && !configFileName.isEmpty()){
            //TODO parse config file and set columnNames
            // Assume same ordering and TODO put in config notes
        }

        if(headerRow != null && headerRow.length > 0){
            for(int headerRowIndex = 0; headerRowIndex < headerRow.length; headerRowIndex++) {
                for(int columnNamesIndex = 0; columnNamesIndex < columnNames.length; columnNamesIndex++) {
                    if (columnNames[columnNamesIndex].equals(headerRow[headerRowIndex])) {
                        headerToIndexMap.put(defaultColumnNames[columnNamesIndex], headerRowIndex);
                    }
                }
            }
        }

        return headerToIndexMap;
    }

    /**
     *
     * Gets the value of a row based on the column and header map and validates it.
     *
     * @param row
     * @param columnName
     * @param headerToIndexMap
     * @param requiredField
     * @return the validated value
     * @throws org.bch.myscilhs.csv.service.CSVUploadService.CSVException if required column is missing
     * or required value is empty
     */
    protected String getColumnValue(final String[] row, String columnName, final Map<String, Integer> headerToIndexMap,
                                    boolean requiredField) throws CSVException{
        String value = null;
        Integer columnIndex = headerToIndexMap.get(columnName);
        if (columnIndex == null) {
            if(requiredField){
                logger.error("Could not find required field: " + columnName);
                throw new CSVException("Could not find required field: " + columnName);
            }
            return value;
        }
        value = row[columnIndex];
        if((value == null || value.isEmpty()) && requiredField){
            logger.error("Required field empty: " + columnName);
            throw new CSVException("Required field empty: " + columnName);
        }
        return value;
    }

    /**
     *
     * Verifies known required headers are present.
     *
     * @param headerToIndexMap
     * @param result
     * @return
     */
    protected static boolean validateHeaders(final Map<String, Integer> headerToIndexMap, PatientFileResult result) {
        if(headerToIndexMap.get(defaultColumnNames[0]) == null){
            result.getFileLineToErrorsMap().put(1L, "Could not find header matching " + defaultColumnNames[0]
                    + ". Please check file or configuration.");
           return false;
        }
        if(headerToIndexMap.get(defaultColumnNames[1]) == null){
            result.getFileLineToErrorsMap().put(1L, "Could not find header matching " + defaultColumnNames[1]
                    + ". Please check file or configuration.");
            return false;
        }
        if(headerToIndexMap.get(defaultColumnNames[2]) == null){
            result.getFileLineToErrorsMap().put(1L, "Could not find header matching " + defaultColumnNames[2]
                    + ". Please check file or configuration.");
            return false;
        }
        if(headerToIndexMap.get(defaultColumnNames[3]) == null){
            result.getFileLineToErrorsMap().put(1L, "Could not find header matching " + defaultColumnNames[3]
                    + ". Please check file or configuration.");
            return false;
        }

        if(headerToIndexMap.get(defaultColumnNames[6]) == null &&
                headerToIndexMap.get(defaultColumnNames[7]) == null &&
                headerToIndexMap.get(defaultColumnNames[8]) == null &&
                headerToIndexMap.get(defaultColumnNames[9]) == null){
            result.getFileLineToErrorsMap().put(1L,
                    "Could not find header matching one of the following " + defaultColumnNames[6]
                    + ", " + defaultColumnNames[7]
                    + ", " + defaultColumnNames[8]
                    + ", " + defaultColumnNames[9]
                    + ". Please check file or configuration.");
            return false;
        }
        return true;
    }

    /**
     *
     * Verifies at least one of the passed Strings is nonNull and nonEmpty
     *
     * @param fields
     * @return
     */
    protected static boolean verifyOneExists(final String... fields) {
        for(String field : fields){
            if (field != null && !field.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    protected class CSVException extends Exception{

        public CSVException(){
            super("Unknown CSV Exception");
        }

        public CSVException(String message){
            super(message);
        }
    }
}
