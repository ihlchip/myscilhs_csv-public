/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.service;

import org.bch.myscilhs.csv.pojo.Patient;

import java.util.List;
import java.util.Map;

/**
 * Created by paul
 */
public class PatientFileResult {
    private final Map<Long, String> fileLineToErrorsMap;
    private final List<Patient> patientList;

    public PatientFileResult(Map<Long, String> fileLineToErrorsMap, List<Patient> patientList) {
        this.fileLineToErrorsMap = fileLineToErrorsMap;
        this.patientList = patientList;
    }

    public Map<Long, String> getFileLineToErrorsMap() {
        return fileLineToErrorsMap;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    @Override
    public String toString() {
        return "PatientFileResult{" +
                "fileLineToErrorsMap=" + fileLineToErrorsMap +
                ", patientList=" + patientList +
                '}';
    }
}
