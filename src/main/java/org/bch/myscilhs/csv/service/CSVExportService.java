/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.service;

import org.bch.myscilhs.csv.pojo.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

/**
 * Exports patient list and public ids the to a given file location
 */
@Service
public class CSVExportService {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

  private static final String[] defaultColumnNames = {"subject_id", "record_id",
                                                      "myscilhs_contact_email", "survey_id",
                                                      "survey_version", "completion_timestamp",
                                                      "myscilhs_study_data_complete", "surveytype",
                                                      "f_name", "ped_f_name", "mid_name",
                                                      "ped_mid_name","l_name","ped_l_name","dob",
                                                      "ped_dob","gender","ped_gender","home_phone",
                                                      "ped_home_phone","mobile_phone","ped_mobile_phone",
                                                      "email","ped_email","st_add1","ped_st_add1",
                                                      "st_add2","ped_st_add2","city","ped_city","state",
                                                      "ped_state","zip","ped_zip","ans_phone","nopickup_date",
                                                      "speak_part","speak_part_othcom","callback",
                                                      "confirm_number","answ_machine_response","verify_adult",
                                                      "verify_ped","recieve_optout","read_letter",
                                                      "resendletter","administerconsent","consent",
                                                      "no_participate","anyquestions","race___1",
                                                      "race___black","race___2","race___hispanic",
                                                      "race___3","race___4","race___5","race___6",
                                                      "race___7","ped_race___1","ped_race___black",
                                                      "ped_race___2","ped_race___hispanic",
                                                      "ped_race___3","ped_race___4","ped_race___5",
                                                      "ped_race___6","ped_race___7","race_oth",
                                                      "ped_race_othcom","high_degree","numinhouse",
                                                      "under18inhome","marital_stat","part_future_res",
                                                      "ped_part_future_res","contactinfo_match",
                                                      "ped_contactinfo","contact_wrong","parent_f_name",
                                                      "parent_l_name","part_res_before","res_oth_comm",
                                                      "finish_live_int","profile_complete","genhlth",
                                                      "qol","phyhealth","sob_sitting","sob_washed",
                                                      "sob_walkhome","walk2_flat","walk2_incline",
                                                      "breathless_talk","exhaustedeasy","prefcontact___1",
                                                      "prefcontact___2","prefcontact___3","prefcontact___4",
                                                      "prefcontact___5","prefcontact___6","prefcontact___7",
                                                      "prefcontact___8","prefcontact___9","othermed_prefcontact",
                                                      "survey_2ormore","bloodsample","phoneint","takemed",
                                                      "commcenter_school","othsinfamily","hospstay",
                                                      "use_telephony","telephony_q2","pah_inbound_ivr_adult_survey_complete",
                                                      "schoolmissed","child_part_sports","activity_restrictions",
                                                      "ped_genhlth","ped_gol","ped_phyhealth","ped_sob_sitting",
                                                      "ped_sob_washed","ped_sob_walkhome","ped_walk2_flat",
                                                      "ped_sob_sports","ped_exhasteasy","ped_prefcontact___1",
                                                      "ped_prefcontact___2","ped_prefcontact___3,",
                                                      "ped_prefcontact___4","ped_prefcontact___5",
                                                      "ped_prefcontact___6","ped_prefcontact___7",
                                                      "ped_prefcontact___8","ped_prefcontact___9",
                                                      "ped_othmed_prefcont","ped_survey_2ormore",
                                                      "ped_bloodsample","ped_phoneint","ped_takemed",
                                                      "ped_commcen_school","ped_othsinfamily",
                                                      "ped_hospstay","use_telphony_peds","telephony_q2_ped",
                                                      "pah_inbound_ivr_ped_proxy_survey_complete"};
  private static final String fileSuffix = ".csv";
  private static final DateFormat sdfFile = new SimpleDateFormat("YYYY-MM-dd_HH_mm_ss");
  static final String DEFAULT_DELIMITER = ",";

  private static Logger logger = LoggerFactory.getLogger(CSVExportService.class);

  /**
   *
   * Saves the patientList to the specified location as a csv file as defined by configuration.
   *
   * @param patientList
   * @param location
   * @param i2b2IdToPublicId
   * @return true if the file was successfully saved to disk, false otherwise
   * @throws IOException
   */
  public boolean exportPatientFileLocal(List<Patient> patientList, String location,
                                        Map<String, String> i2b2IdToPublicId) throws IOException {
    boolean result = false;

    if(patientList == null || patientList.isEmpty()){
      return result;
    }

    String fileName = generateExportFileName();

    Writer writer = null;

    try {
      writer = new BufferedWriter(new OutputStreamWriter(
          new FileOutputStream(location+fileName), "utf-8"));
      //Write header
      for(String header: defaultColumnNames){
        writer.write(header + ((!header.equals(defaultColumnNames[defaultColumnNames.length - 1]))
                               ? DEFAULT_DELIMITER : ""));
      }
      writer.write("\n");

      for(Patient patient : patientList){
        // subject email id version
        writer.write(buildLine(patient, i2b2IdToPublicId.get(patient.getPatientExtId())));
        writer.write("\n");
      }
    } catch (IOException e) {
      logger.error("Failed to write file!", e);
    } finally {
      try {
        writer.close();
      } catch (Exception ex) {
        logger.trace("Failed to close csv file.");
      }
    }
    return result;
  }

  String generateExportFileName(){
    Date now = new Date();
    return sdfFile.format(now) + fileSuffix;
  }

  String buildLine(Patient patient, String publicId){
    StringBuilder sb = new StringBuilder();
    // 32 columns with potential information, The rest are placeholders
    sb.append(publicId);
    sb.append(DEFAULT_DELIMITER+patient.getPatientExtId());
    sb.append(DEFAULT_DELIMITER+patient.getEmail());
    sb.append(DEFAULT_DELIMITER/**+surveyId**/);
    sb.append(DEFAULT_DELIMITER/**+surveyVersion**/);
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getFirstName());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getMiddleName());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getLastName());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+sdf.format(patient.getDateOfBirth()));
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getHomePhone());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getDayPhone());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getEmail());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getAddress1());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getAddress2());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getCity());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getState());
    sb.append(DEFAULT_DELIMITER);
    sb.append(DEFAULT_DELIMITER+patient.getZipCode());
    sb.append(DEFAULT_DELIMITER);
    for(int i=0; i < defaultColumnNames.length - 33; i++){
      sb.append(DEFAULT_DELIMITER);
    }
    return sb.toString();
  }
}
