/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.service;

import org.bch.myscilhs.csv.dao.PublicIDDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Service level access to public id and private id mappings
 */
@Service
@RequestMapping("/rest/public_id")
@RestController
public class PublicIdService {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
  private static final DateFormat sdfFile = new SimpleDateFormat("YYYY-MM-dd_hh_mm_ss");

  private static Logger logger = LoggerFactory.getLogger(PublicIdService.class);

  private final PublicIDDao publicIDDao;

  @Autowired
  public PublicIdService(PublicIDDao publicIDDao) {
    this.publicIDDao = publicIDDao;
  }

  @RequestMapping("/i2b2_id")
  @ResponseBody
  public ResponseEntity<String> i2b2IdFromPublicId(@RequestParam(value="publicId") String publicId) {
    String response = "";
    try{
      response = publicIDDao.fetchI2B2Id( publicId);
    } catch (Exception e){
      if(logger.isErrorEnabled()){
        logger.error("DAO fetch failed:", e);
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
      }
    }
    return new ResponseEntity<String>(response, HttpStatus.OK);
  }
}
