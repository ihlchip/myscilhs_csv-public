/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.spring;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Initialize the filter chain
 */
public class SpringSecInitializer  extends AbstractSecurityWebApplicationInitializer {

}
