/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.spring;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.reloading.ReloadingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.naming.NamingException;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SpringSecConfig extends WebSecurityConfigurerAdapter {
  private static final String PROP_APP_USER = "myscilhs.app.user";
  private static final String PROP_APP_PASS = "myscilhs.app.pass";
  private Logger logger = LoggerFactory.getLogger(SpringSecConfig.class);

  @Bean
  public ReloadingStrategy secReloadingStrategy() {
    FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
    strategy.setRefreshDelay(1000);
    return strategy;
  }

  @Bean
  public PropertiesConfiguration secProperties() {
    try {
      JndiTemplate jndiTemplate = new JndiTemplate();
      String propertiesFile = jndiTemplate.lookup("java:comp/env/myscilhsCSVConfigFile", String.class);
      if (propertiesFile == null) {
        throw new RuntimeException("Could not find mySCILHS properties file string from JNDI");
      } else {
        PropertiesConfiguration pc = new PropertiesConfiguration(propertiesFile);
        pc.setReloadingStrategy(secReloadingStrategy());
        return pc;
      }
    } catch (ConfigurationException ce) {
      logger.error("Cannot access application properties file. Check server and web application configuration.", ce);
      throw new RuntimeException(ce);
    } catch (NamingException ne) {
      logger.error("Problem doing JNDI lookup of myscilhs configuration path", ne);
      throw new RuntimeException(ne);
    }
  }

  @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      PropertiesConfiguration pc = secProperties();
      auth
          .inMemoryAuthentication().withUser(pc.getString(PROP_APP_USER)).password(pc.getString(PROP_APP_PASS)).roles("USER", "ADMIN");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
      return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http
          .csrf().disable()
          .authorizeRequests()
//          .antMatchers("/login*").permitAll()
          .antMatchers("/**").access("hasRole('ROLE_USER')")
          .and().formLogin()
          .defaultSuccessUrl("/csv_upload", true);
//          .loginPage("/login")
//          .failureUrl("/login?error=true")
//          .and().logout()
//          .logoutSuccessUrl("/login?logout");
    }


  /**
   * exclude the rest call from security until I can figure out how to auth it properly.
   *
   * @param web
   * @throws Exception
   */
  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/rest/**");
  }
}
