/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.spring;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.reloading.ReloadingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.naming.NamingException;

@Configuration
//@PropertySource("classpath:myscilhs_csv.properties")
@ComponentScan(basePackages = {"org.bch.myscilhs.csv"})
@Import({ SpringSecConfig.class })
public class AppConfig {

  private static final String PROP_DB_TYPE = "myscilhs.db.type";
  private static final String PROP_DB_DRIVER = "myscilhs.db.driver";
  private static final String PROP_DB_HOST = "myscilhs.db.host";
  private static final String PROP_DB_PORT = "myscilhs.db.port";
  private static final String PROP_DB_SCHEMA = "myscilhs.db.schema";
  private static final String PROP_DB_USER = "myscilhs.db.user";
  private static final String PROP_DB_PASS = "myscilhs.db.pass";

  private final Logger logger = LoggerFactory.getLogger(AppConfig.class);

  @Bean
  public ReloadingStrategy reloadingStrategy() {
    FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
    strategy.setRefreshDelay(1000);
    return strategy;
  }

  @Bean
  public PropertiesConfiguration applicationProperties() {
    try {
      JndiTemplate jndiTemplate = new JndiTemplate();
      String propertiesFile = jndiTemplate.lookup("java:comp/env/myscilhsCSVConfigFile", String.class);
      if (propertiesFile == null) {
        throw new RuntimeException("Could not find mySCILHS properties file string from JNDI");
      } else {
        PropertiesConfiguration pc = new PropertiesConfiguration(propertiesFile);
        pc.setReloadingStrategy(reloadingStrategy());
        return pc;
      }
    } catch (ConfigurationException ce) {
      logger.error("Cannot access application properties file. Check server and web application configuration.", ce);
      throw new RuntimeException(ce);
    } catch (NamingException ne) {
      logger.error("Problem doing JNDI lookup of myscilhs configuration path", ne);
      throw new RuntimeException(ne);
    }
  }

  @Bean
  public CommonsMultipartResolver multipartResolver() {
    return new CommonsMultipartResolver();
  }

  @Bean
  public DriverManagerDataSource myscilhsDataSource() {
    PropertiesConfiguration pc = applicationProperties();
    DriverManagerDataSource ds = new DriverManagerDataSource("jdbc:"+pc.getString(PROP_DB_TYPE)+"://"+pc.getString(
        PROP_DB_HOST)+":"+pc.getString(PROP_DB_PORT)+"/"+pc.getString(PROP_DB_SCHEMA),
                                       pc.getString(PROP_DB_USER),
                                       pc.getString(PROP_DB_PASS));
    ds.setDriverClassName(pc.getString(PROP_DB_DRIVER));
    return ds;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(DriverManagerDataSource myscilhsDataSource){
    return new JdbcTemplate(myscilhsDataSource);
  }

  @Bean
  public JdbcTemplate jdbcInsertTemplate(DriverManagerDataSource myscilhsDataSource){
    return new JdbcTemplate(myscilhsDataSource);
  }

  @Bean
  public DataSourceTransactionManager publicIdTransactionManager(DriverManagerDataSource myscilhsDataSource){
    return new DataSourceTransactionManager(myscilhsDataSource);
  }
}


