/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.controller;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.bch.myscilhs.csv.dao.PublicIDDao;
import org.bch.myscilhs.csv.pojo.Patient;
import org.bch.myscilhs.csv.service.CSVExportService;
import org.bch.myscilhs.csv.service.CSVUploadService;
import org.bch.myscilhs.csv.service.PatientFileResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Simple CSV Upload servlet. Displays the upload form and sends 
 * submitted forms to the CSV service.
 */

@Controller
@RequestMapping("/csv_upload")
public final class CSVUploadController {

  private static final String CSV_EXPORT_LOCATION_PROP = "myscilhs.redcap.export.location";

  protected static Logger logger = LoggerFactory.getLogger(CSVUploadController.class);

  private CSVUploadService csvUploadService;
  private CSVExportService csvExportService;
  private PublicIDDao publicIDDao;

  @Resource
  PropertiesConfiguration applicationProperties;

  @Autowired
  public CSVUploadController(CSVUploadService csvUploadService, CSVExportService csvExportService,
                             PublicIDDao publicIDDao){
    this.csvUploadService = csvUploadService;
    this.csvExportService = csvExportService;
    this.publicIDDao = publicIDDao;
  }

  /**
   * Show the csv upload form.
   *
   * @param request The servlet request we are processing
   * @param response The servlet response we are producing
   *
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet error occurs
   */
  @RequestMapping(method = RequestMethod.GET)
  public ModelAndView doGet(HttpServletRequest request,
                            HttpServletResponse response)
      throws IOException, ServletException {

    ModelAndView model = new ModelAndView("csv_upload");
    return model;
  }

  /**
   * Receive the CSV upload form and process.
   *
   * @param request The servlet request we are processing
   * @param response The servlet response we are producing
   *
   * @exception IOException if an input/output error occurs
   */
  @RequestMapping(method = RequestMethod.POST, headers = "content-type=multipart/*")
  public ModelAndView doPost(MultipartHttpServletRequest request,
                             HttpServletResponse response) throws IOException {
    ModelAndView model = new ModelAndView("csv_upload");


    String campaignId;
    // TODO: Find the FileName from the request.
    String configFileName = null;
    MultipartFile csvFile = request.getFile("payload");
    String surveyId = request.getParameter("surveyId");
    //String surveyVersion = request.getParameter("surveyVersion");

    /** if(surveyId == null || surveyId.isEmpty() || surveyVersion == null || surveyVersion.isEmpty()){
      model.addObject("fileError", "Both Survey Id and Survey Version are required.");
      return model;
    } **/

    if(logger.isDebugEnabled()) {
      logger.debug("CSVFile size " + ((csvFile == null) ? 0 : csvFile.getSize()) + " bytes.");
    }

    PatientFileResult result = null;
    String errorMsg = null;

    Map<String, String> publidIdMap = null;
    // Load the patients
    try {
      result = csvUploadService.loadPatientFile(csvFile, configFileName);

    } catch (IOException ioe) {
      errorMsg = "Could not read file!";
      if(logger.isErrorEnabled()) {
        logger.error(errorMsg);
      }
    } catch (Exception e) {
      errorMsg = "Error parsing file: " + e.getMessage();
      if(logger.isErrorEnabled()) {
        logger.error(errorMsg);
      }
    }

    if (errorMsg != null && !errorMsg.isEmpty()) {
      model.addObject("fileError", errorMsg);
      return model;
    }

    // Assign public ids
    try{
      publidIdMap = publicIDDao.generatePublicIds(result.getPatientList());
    } catch(Exception e){
      errorMsg = "Error generating public ids: " + e.getMessage();
      if(logger.isErrorEnabled()) {
        logger.error(errorMsg);
      }
    }

    if (errorMsg != null && !errorMsg.isEmpty()) {
      model.addObject("fileError", errorMsg);
      return model;
    }

    // Export File
    try{
      String exportLocation = applicationProperties.getString(CSV_EXPORT_LOCATION_PROP);
      boolean fileSaveResult = csvExportService.exportPatientFileLocal(result.getPatientList(),
                                                                       exportLocation,
                                                                       publidIdMap);

    } catch(Exception e){
      errorMsg = "Error saving csv file: " + e.getMessage();
      if(logger.isErrorEnabled()) {
        logger.error(errorMsg);
      }
    }

    // Save public ids
    try{
      publicIDDao.savePublicIds(publidIdMap);
    } catch(Exception e){
      errorMsg = "Error saving public ids: " + e.getMessage();
      if(logger.isErrorEnabled()) {
        logger.error(errorMsg);
      }
    }

    if (errorMsg != null && !errorMsg.isEmpty()) {
      model.addObject("fileError", errorMsg);
      return model;
    }

    if (errorMsg != null && !errorMsg.isEmpty()) {
      model.addObject("fileError", errorMsg);
      return model;
    }
    if (result.getFileLineToErrorsMap() != null && !result.getFileLineToErrorsMap().isEmpty()) {
      model.addObject("lineErrors", prettyPrintErrorMap(result.getFileLineToErrorsMap()));
    }
    if (result.getPatientList() != null && !result.getPatientList().isEmpty()) {
      model.addObject("patientList", prettyPrintPatientList(result.getPatientList(), publidIdMap));
      if(logger.isDebugEnabled()) {
        logger.debug("Patient List: " + result.getPatientList().toString());
      }
    } else {
      if(logger.isDebugEnabled()) {
        logger.debug("No patients loaded.");
      }
      model.addObject("patientList", "No patients loaded.");
    }
    return model;
  }

  /**
   *
   * Formats a nonNull nonEmpty errorMap for display. Assumes the underlying implementation is a LinkedHashMap for
   * errors to always print in line order.
   *
   * @param fileErrorsMap
   * @return
   */
  protected static String prettyPrintErrorMap(final Map<Long, String> fileErrorsMap){

    // StringBuilder because we don't need to synchronize here.
    StringBuilder sb = new StringBuilder();
    for(Long line : fileErrorsMap.keySet()){
      sb.append("Line \'");
      sb.append(line);
      sb.append("\' reported error: ");
      sb.append(fileErrorsMap.get(line));
      sb.append("<br />");
    }
    return sb.toString();
  }

  /**
   *
   * Formats a nonNull nonEmpty patientList for display.
   *
   * @param patientList
   * @return
   */
  protected static String prettyPrintPatientList(final List<Patient> patientList,
                                                 final Map<String, String> publidIdMap){

    // StringBuilder because we don't need to synchronize here.
    StringBuilder sb = new StringBuilder();
    for(Patient p : patientList){
      sb.append("Patient Id:  \'");
      sb.append(p.getPatientExtId());
      sb.append("\' Public Id \'");
      sb.append(publidIdMap.get(p.getPatientExtId()));
      sb.append("\' successfully loaded.");
      sb.append("<br />");
    }
    return sb.toString();
  }
} 
