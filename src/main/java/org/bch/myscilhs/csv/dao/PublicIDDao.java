/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.dao;

import org.bch.myscilhs.csv.pojo.Patient;

import java.util.List;
import java.util.Map;

/**
 * Generates and stores public ids based on the Survey Id and Version
 */
public interface PublicIDDao {

  public Map<String, String> generatePublicIds(List<Patient> patientList);
  public void savePublicIds(Map<String, String> privateToPublicIds);
  public String fetchI2B2Id(String publicId);
}
