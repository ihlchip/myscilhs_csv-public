/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.csv.dao;

import org.bch.myscilhs.csv.pojo.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Generates and stores public ids based on the Survey Id and Version
 */
@Component
public class PublicIDDaoJdbcImpl implements PublicIDDao{

  private static final String getI2B2IdQuery = "select i2b2_id from public_id_mapping where public_id = ?";
  private static final String insertPrivateIdQuery = "insert into public_id_mapping (i2b2_id, public_id) values (?, ?)";

  JdbcTemplate jdbcTemplate;
  JdbcTemplate jdbcInsertTemplate;

  @Autowired
  PublicIDDaoJdbcImpl(JdbcTemplate jdbcTemplate, JdbcTemplate jdbcInsertTemplate){
   this.jdbcTemplate = jdbcTemplate;
   this.jdbcInsertTemplate = jdbcInsertTemplate;
  }

  @Override
  public Map<String, String> generatePublicIds(List<Patient> patientList){
    Map<String, String> publicIdMap = new HashMap<String, String>(patientList.size());

    for(Patient patient : patientList){
      String publicId = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
      publicIdMap.put(patient.getPatientExtId(), publicId);
    }
    return publicIdMap;
  }

  @Override
  @Transactional("publicIdTransactionManager")
  public void savePublicIds(Map<String, String> publicIdMap){
    for(String privateId : publicIdMap.keySet()){
      jdbcInsertTemplate.update(insertPrivateIdQuery, privateId, publicIdMap.get(privateId));
    }
  }

  @Override
  public String fetchI2B2Id(String publicId) {
    String i2b2Id = this.jdbcTemplate.queryForObject(getI2B2IdQuery, String.class, publicId);
    return i2b2Id;
  }
}
