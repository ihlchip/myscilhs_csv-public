<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>Patient CSV Upload</title>
<style>
.file-error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}
.error-box {
	width: 355px;
	padding: 20px;
	margin: 33px auto;
	background: #f2dede;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
.patient-box {
	width: 355px;
	padding: 20px;
	margin: 33px auto;
	background: #d9edf7;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
#upload-box {
	width: 355px;
	padding: 20px;
	margin: 33px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>
</head>
<body>
    <div id="upload-box">
         <c:if test="${not empty fileError}">
       		<div class="file-error">${fileError}</div>
       	</c:if>
        <h3>Patient File Upload:</h3>
        <form method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td>Survey Id:</td>
                    <td><input type='text' name='surveyId' value=''></td>
                </tr>
            </table>
            <input type="file" name="payload" size="50" />
            <br /> <br />
            <input type="submit" value="Upload File" />
        </form>
        <p align="right"><a href="<c:url value="/logout" />">(logout)</a></p>
    </div>
    <c:if test="${not empty lineErrors}">
   		<div class="error-box">${lineErrors}</div>
   	</c:if>
   	<c:if test="${not empty patientList}">
   		<div class="patient-box">${patientList}</div>
   	</c:if>
</body>
</html>
